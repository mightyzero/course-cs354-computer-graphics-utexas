#ifndef CORE_COMMON_H_
#define CORE_COMMON_H_

#include <assert.h>
#include <stdint.h>

#define Assert assert

// Useful constants
#define PI      3.14159265359
#define INV_PI  0.31830988618
#define TWO_PI  6.28318530718
#define INV_180 0.00555555555

#endif
