// The main ray tracer.

#pragma warning (disable: 4786)

#include "RayTracer.h"
#include "scene/light.h"
#include "scene/material.h"
#include "scene/ray.h"

#include "parser/Tokenizer.h"
#include "parser/Parser.h"

#include "ui/TraceUI.h"
#include <cmath>
#include <algorithm>
#include <assert.h>

extern TraceUI* traceUI;

#include <iostream>
#include <fstream>

using namespace std;

// Use this variable to decide if you want to print out
// debugging messages.  Gets set in the "trace single ray" mode
// in TraceGLWindow, for example.
bool debugMode = false;

// Trace a top-level ray through normalized window coordinates (x,y)
// through the projection plane, and out into the scene.  All we do is
// enter the main ray-tracing method, getting things started by plugging
// in an initial ray weight of (0.0,0.0,0.0) and an initial recursion depth of 0.
Vec3d RayTracer::trace(double x, double y) {
  // Clear out the ray cache in the scene for debugging purposes,
  scene->intersectCache.clear();

  ray r(Vec3d(0, 0, 0), Vec3d(0, 0, 0), ray::VISIBILITY);

  scene->getCamera().rayThrough(x, y, r);
  Vec3d ret = traceRay(r, Vec3d(1.0, 1.0, 1.0), 0);
  ret.clamp();
  return ret;
}

// Do recursive ray tracing!  You'll want to insert a lot of code here
// (or places called from here) to handle reflection, refraction, etc etc.
Vec3d RayTracer::traceRay(const ray& r, const Vec3d& thresh, int depth) {
  isect i;

  if (scene->intersect(r, i)) {
    // YOUR CODE HERE

    // An intersection occured!  We've got work to do.  For now,
    // this code gets the material for the surface that was intersected,
    // and asks that material to provide a color for the ray.

    // This is a great place to insert code for recursive ray tracing.
    // Instead of just returning the result of shade(), add some
    // more steps: add in the contributions from reflected and refracted
    // rays.

    Vec3d isectPoint = r.at(i.t);     // Intersection point
    const Material& m = i.getMaterial();

    if (depth >= traceUI->getDepth()) {
      // Maxium depth reached: Return color of the point hit
      return m.shade(scene, r, i);

    } else {
      // Maxium depth not reached: Spawn new refraction/reflection rays
      // Incident, normal, reflection, and transmission vector
      Vec3d I, N, R, T;
      Vec3d ICos, ISin;     // Cosine, sine vectors of incident vector
      Vec3d TCos, TSin;     // Cosine, sine vectors of transmission vector
      double IR, TR;        // Incident, Transmission Refraction Index
      double RRI;           // Ratio of Refraction Indices
      Vec3d Intensity;      // Sum contributions of all visible rays

      if (r.type() != ray::SHADOW) {
        // Start intensity at local illumination level
        Intensity = m.shade(scene, r, i);

        //==========[ Compute Reflection ]=====================================
        // Get incident and normal vectors
        I = -r.getDirection();        // Incident vector
        if (depth % 2 == 0)
          N = i.N;
        else
          N = -i.N;                   // Normal vector

        // Compute cosine, sine vectors of incident vector
        ICos = (I * N) * N;           // Cosine vector of incident vector
        ISin = ICos - I;              // Sine vector of incident vector

        if (debugMode) {
          std::cout << "N: " << N << "\t" << endl;
          std::cout << "I: " << I << "\t" << endl;
          std::cout << "I * N: " << I * N << "\t" << endl;
          std::cout << "ICos: " << ICos << "\t" << endl;
        }
        
        // Compute reflection vector
        R = ICos + ISin;              // Reflection vector

        // Cast reflection ray
        ray reflection = ray(isectPoint, R, ray::REFLECTION);
        Intensity += m.kr(i) % traceRay(reflection, thresh, depth + 1);

        //==========[ Compute Refraction ]=====================================
        // Compute refraction indices
        if (depth % 2 == 0) {
          // Entering object
          IR = 1.0;
          TR = m.index(i);
        }
        else {
          // Exiting object
          IR = m.index(i);
          TR = 1.0;
        }
        RRI = IR / TR;

        // Compute sine vectors of transmission vector
        TSin = RRI * ISin;

        if (debugMode) {
//           std::cout << "i.t: " << i.t << "\t";
//           std::cout << "m.index(i): " << m.index(i) << "\t";
//           std::cout << "RRI: " << RRI << "\t";
//           std::cout << "TSin: " << TSin << "\t";
//           std::cout << "TSin.length2: " << TSin.length2() << "\t" << std::endl;
        }

        // Cast new refraction ray
        if ((m.kt(i)[0] > 0 || m.kt(i)[1] > 0 || m.kt(i)[2] > 0) &&
            (TSin.length2() < 1)) {
          // Total internal reflection not achieved
          TCos = sqrt(1 - TSin.length2()) * (-N);
          T = TCos + TSin;
//           cout << "T.length() " << T.length() << endl;
          ray transmission = ray(isectPoint, T, ray::REFRACTION);
          Intensity += m.kt(i) % traceRay(transmission, thresh, depth + 1);
        }

        //=====================================================================
        //==========[ Debugging Normals ]======================================
        if (debugMode) {
          std::cout << "N: " << N << endl;
        }

        return Intensity;
      }
      else {
        return Vec3d(0.0, 0.0, 0.0);
      }
    }

  } else {
    // No intersection.  This ray travels to infinity, so we color
    // it according to the background color, which in this (simple) case
    // is just black.

    return Vec3d(0.0, 0.0, 0.0);
  }
}

RayTracer::RayTracer()
  : scene(0), buffer(0), buffer_width(256), buffer_height(256), m_bBufferReady(false) {

}


RayTracer::~RayTracer() {
  delete scene;
  delete [] buffer;
}

void RayTracer::getBuffer(unsigned char *&buf, int & w, int & h) {
  buf = buffer;
  w = buffer_width;
  h = buffer_height;
}

double RayTracer::aspectRatio() {
  return sceneLoaded() ? scene->getCamera().getAspectRatio() : 1;
}

bool RayTracer::loadScene(char * fn) {
  ifstream ifs(fn);

  if (!ifs) {
    string msg("Error: couldn't read scene file ");
    msg.append(fn);
    traceUI->alert(msg);
    return false;
  }

  // Strip off filename, leaving only the path:
  string path(fn);

  if (path.find_last_of("\\/") == string::npos)
    path = ".";
  else
    path = path.substr(0, path.find_last_of("\\/"));

  // Call this with 'true' for debug output from the tokenizer
  Tokenizer tokenizer(ifs, false);
  Parser parser(tokenizer, path);

  try {
    delete scene;
    scene = 0;
    scene = parser.parseScene();
  } catch (SyntaxErrorException& pe) {
    traceUI->alert(pe.formattedMessage());
    return false;
  } catch (ParserException& pe) {
    string msg("Parser: fatal exception ");
    msg.append(pe.message());
    traceUI->alert(msg);
    return false;
  } catch (TextureMapException e) {
    string msg("Texture mapping exception: ");
    msg.append(e.message());
    traceUI->alert(msg);
    return false;
  }

  if (! sceneLoaded())
    return false;

  return true;
}

void RayTracer::traceSetup(int w, int h) {
  if (buffer_width != w || buffer_height != h) {
    buffer_width = w;
    buffer_height = h;

    bufferSize = buffer_width * buffer_height * 3;
    delete [] buffer;
    buffer = new unsigned char[ bufferSize ];
  }

  memset(buffer, 0, w * h * 3);
  m_bBufferReady = true;
}

void RayTracer::tracePixel(int i, int j) {
  Vec3d col;

  if (! sceneLoaded())
    return;

  double x = double(i) / double(buffer_width);
  double y = double(j) / double(buffer_height);

  col = trace(x, y);

  unsigned char *pixel = buffer + (i + j * buffer_width) * 3;

  pixel[0] = (int)(255.0 * col[0]);
  pixel[1] = (int)(255.0 * col[1]);
  pixel[2] = (int)(255.0 * col[2]);
}



